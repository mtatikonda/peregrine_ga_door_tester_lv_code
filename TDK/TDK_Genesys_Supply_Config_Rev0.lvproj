﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="24008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="TDK_Genesys_Supply_Config_Rev0.vi" Type="VI" URL="../TDK_Genesys_Supply_Config_Rev0.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Abort Transient.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Abort Transient.vi"/>
				<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Close.vi"/>
				<Item Name="Configure Autostart.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Autostart.vi"/>
				<Item Name="Configure Communication Remote.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Communication Remote.vi"/>
				<Item Name="Configure Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Current Limit.vi"/>
				<Item Name="Configure Current Slew.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Current Slew.vi"/>
				<Item Name="Configure Current Source.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Current Source.vi"/>
				<Item Name="Configure Display.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Display.vi"/>
				<Item Name="Configure FoldBack Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure FoldBack Protection.vi"/>
				<Item Name="Configure Inhibit Enable.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Inhibit Enable.vi"/>
				<Item Name="Configure Interlock.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Interlock.vi"/>
				<Item Name="Configure Internal Resistance.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Internal Resistance.vi"/>
				<Item Name="Configure List (Current).vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/List/Configure List (Current).vi"/>
				<Item Name="Configure List (Voltage).vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/List/Configure List (Voltage).vi"/>
				<Item Name="Configure List.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/List/Configure List.vi"/>
				<Item Name="Configure Output Pins.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Output Pins.vi"/>
				<Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Output.vi"/>
				<Item Name="Configure Over Voltage Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Over Voltage Protection.vi"/>
				<Item Name="Configure Power Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Power Limit.vi"/>
				<Item Name="Configure Preload.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Preload.vi"/>
				<Item Name="Configure Range.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Range.vi"/>
				<Item Name="Configure Sense.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Sense.vi"/>
				<Item Name="Configure Slew Control Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Slew Control Mode.vi"/>
				<Item Name="Configure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Trigger.vi"/>
				<Item Name="Configure Under Voltage Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Under Voltage Protection.vi"/>
				<Item Name="Configure Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Voltage Limit.vi"/>
				<Item Name="Configure Voltage Slew.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Voltage Slew.vi"/>
				<Item Name="Configure Voltage Source.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Configure Voltage Source.vi"/>
				<Item Name="Configure Waveform (Current).vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Waveform/Configure Waveform (Current).vi"/>
				<Item Name="Configure Waveform (Voltage).vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Waveform/Configure Waveform (Voltage).vi"/>
				<Item Name="Configure Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Configure/Waveform/Configure Waveform.vi"/>
				<Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Error Query.vi"/>
				<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Initialize.vi"/>
				<Item Name="Initiate Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Initiate Trigger.vi"/>
				<Item Name="Multi Addressing.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Multi Addressing.vi"/>
				<Item Name="Operational Condition Register.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Operational Condition Register.vi"/>
				<Item Name="Preset.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Preset.vi"/>
				<Item Name="Query Operation Time.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Query Operation Time.vi"/>
				<Item Name="Query Output Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Query Output Mode.vi"/>
				<Item Name="Query Output Status.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Query Output Status.vi"/>
				<Item Name="Query Panel Lock.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Query Panel Lock.vi"/>
				<Item Name="Questionable Condition Register.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Questionable Condition Register.vi"/>
				<Item Name="Read Current Output.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Current Output.vi"/>
				<Item Name="Read Current.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Current.vi"/>
				<Item Name="Read Over Voltage Protection Level.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Over Voltage Protection Level.vi"/>
				<Item Name="Read Power Output.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Power Output.vi"/>
				<Item Name="Read Power.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Power.vi"/>
				<Item Name="Read Under Voltage Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Under Voltage Protection.vi"/>
				<Item Name="Read Voltage Output.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Voltage Output.vi"/>
				<Item Name="Read Voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Data/Read Voltage.vi"/>
				<Item Name="Recall.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Recall.vi"/>
				<Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Reset.vi"/>
				<Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Revision Query.vi"/>
				<Item Name="Save.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Save.vi"/>
				<Item Name="Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Send Software Trigger.vi"/>
				<Item Name="TDK-Lambda G Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/Genesys Plus Drivers/TDK-Lambda G Series.lvlib"/>
				<Item Name="TDK-Lambda G Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/TDK-Lambda G Series.lvlib"/>
				<Item Name="Transient Memory (Load).vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Transient Memory (Load).vi"/>
				<Item Name="Transient Memory (Store).vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Transient Memory (Store).vi"/>
				<Item Name="Transient Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Transient Memory.vi"/>
				<Item Name="Trigger Continuous.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Action-Status/Low Level/Trigger Continuous.vi"/>
				<Item Name="Unit Address.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Unit Address.vi"/>
				<Item Name="Wait.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Wait.vi"/>
				<Item Name="Write Read Instrument.vi" Type="VI" URL="/&lt;instrlib&gt;/TDK-Lambda G Series/Public/Utility/Write Read Instrument.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace One-Sided.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace One-Sided.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Open Access Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Open Access Mode.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="TDK_Genesys_Supply_Config_Rev0" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{A1BD54D9-FFBF-4645-8BCB-94F135DD1A34}</Property>
				<Property Name="App_INI_GUID" Type="Str">{50B14D42-5F49-4621-8CAA-C1AEF2C1865E}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{E9805E3E-711A-4B7D-BF6A-AD98159BDE0D}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">TDK Supply Control Rev0 </Property>
				<Property Name="Bld_buildSpecName" Type="Str">TDK_Genesys_Supply_Config_Rev0</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{DA0CA06C-5788-4766-BCC9-804B0BEAE7E3}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">TDK_Genesys_Supply_Config_Rev0.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{062A9749-2F03-4E1E-8674-6EB30D9C357F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/TDK_Genesys_Supply_Config_Rev0.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">TDK_Genesys_Supply_Config_Rev0</Property>
				<Property Name="TgtF_internalName" Type="Str">TDK_Genesys_Supply_Config_Rev0</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">TDK_Genesys_Supply_Config_Rev0</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{797D2419-8F6E-4646-A2C6-6CC7ACB4DEC7}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">TDK_Genesys_Supply_Config_Rev0.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
