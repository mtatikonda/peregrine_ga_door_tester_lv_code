﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="24008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="dmm_mux_continuous_mode.vi" Type="VI" URL="../SUBVIs/DMM_Switch_combo/DMM_MUX_Loop/dmm_mux_continuous_mode.vi"/>
		<Item Name="latch.vi" Type="VI" URL="../SUBVIs/latch.vi"/>
		<Item Name="NATs_PublishStream.vi" Type="VI" URL="../../NATs/NATs_PublishStream.vi"/>
		<Item Name="nats_reader_example.vi" Type="VI" URL="../../NATs/nats_reader_example.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev09.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev09.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev11.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev11.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev12.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev12.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev14.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev14.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev15.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev15.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev16.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev16.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev17.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev17.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev18.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev18.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev19.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev19.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev20.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev20.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev20_TESTONLY.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev20_TESTONLY.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev21.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev21.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev21_TestOnly.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev21_TestOnly.vi"/>
		<Item Name="Peregrine_GA_Prod_OperatorUI_Rev22.vi" Type="VI" URL="../Peregrine_GA_Prod_OperatorUI_Rev22.vi"/>
		<Item Name="RI_TestCluster_To_ResultString.vi" Type="VI" URL="../SUBVIs/RI_TestCluster_To_ResultString.vi"/>
		<Item Name="settingsConfig.vi" Type="VI" URL="../settingsConfig.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="niDMM Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Abort.vi"/>
				<Item Name="niDMM Acquisition State.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Acquisition State.ctl"/>
				<Item Name="niDMM Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Close.vi"/>
				<Item Name="niDMM Config Measurement.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Config Measurement.vi"/>
				<Item Name="niDMM Configure Measurement Absolute.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Configure Measurement Absolute.vi"/>
				<Item Name="niDMM Configure Measurement Digits.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Configure Measurement Digits.vi"/>
				<Item Name="niDMM Function To IVI Constant.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Function To IVI Constant.vi"/>
				<Item Name="niDMM Function.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Function.ctl"/>
				<Item Name="niDMM Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Initialize.vi"/>
				<Item Name="niDMM Initiate.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Initiate.vi"/>
				<Item Name="niDMM IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM IVI Error Converter.vi"/>
				<Item Name="niDMM Read Status.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Read Status.vi"/>
				<Item Name="niDMM Read.vi" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Read.vi"/>
				<Item Name="niDMM Resolution in Digits.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDMM/nidmm.llb/niDMM Resolution in Digits.ctl"/>
				<Item Name="niSwitch Abort Scan.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Abort Scan.vi"/>
				<Item Name="niSwitch Can Connect Channels.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Can Connect Channels.vi"/>
				<Item Name="niSwitch Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Close.vi"/>
				<Item Name="niSwitch Commit.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Commit.vi"/>
				<Item Name="niSwitch Configure Scan List.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Configure Scan List.vi"/>
				<Item Name="niSwitch Configure Scan Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Configure Scan Trigger.vi"/>
				<Item Name="niSwitch Connect Channels (Multiple).vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Connect Channels (Multiple).vi"/>
				<Item Name="niSwitch Connect Channels (Single).vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Connect Channels (Single).vi"/>
				<Item Name="niSwitch Connect Channels.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Connect Channels.vi"/>
				<Item Name="niSwitch Disconnect All Channels.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Disconnect All Channels.vi"/>
				<Item Name="niSwitch Get Path.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Get Path.vi"/>
				<Item Name="niSwitch Initialize With Topology.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Initialize With Topology.vi"/>
				<Item Name="niSwitch Initiate Scan.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Initiate Scan.vi"/>
				<Item Name="niSwitch IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch IVI Error Converter.vi"/>
				<Item Name="niSwitch Path Capability.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Path Capability.ctl"/>
				<Item Name="niSwitch Scan Advanced Output Ring.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Scan Advanced Output Ring.ctl"/>
				<Item Name="niSwitch Scan Advanced Output.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Scan Advanced Output.ctl"/>
				<Item Name="niSwitch Scan Mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Scan Mode.ctl"/>
				<Item Name="niSwitch Scanner Advanced To IVI Constant.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Scanner Advanced To IVI Constant.vi"/>
				<Item Name="niSwitch Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Send Software Trigger.vi"/>
				<Item Name="niSwitch Set Continuous Scan.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Set Continuous Scan.vi"/>
				<Item Name="niSwitch Topologies.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Topologies.ctl"/>
				<Item Name="niSwitch Trigger Input Ring.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Trigger Input Ring.ctl"/>
				<Item Name="niSwitch Trigger Input.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Trigger Input.ctl"/>
				<Item Name="niSwitch Trigger Source To IVI Constant.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Trigger Source To IVI Constant.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="BioIsFailed.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/BioIsFailed.vi"/>
				<Item Name="CreateNewConfig.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/CreateNewConfig.vi"/>
				<Item Name="CreateTask.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/CreateTask.vi"/>
				<Item Name="DN4_1Darray2DDT.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_1Darray2DDT.vi"/>
				<Item Name="DN4_1DarrayTo2Darray.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_1DarrayTo2Darray.vi"/>
				<Item Name="DN4_2Darray2DDT.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_2Darray2DDT.vi"/>
				<Item Name="DN4_2DarrayTo1Darray.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_2DarrayTo1Darray.vi"/>
				<Item Name="DN4_AI_InstantRead.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_InstantRead.vi"/>
				<Item Name="DN4_AI_OneBufferedRead.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_OneBufferedRead.vi"/>
				<Item Name="DN4_AI_ReadData.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_ReadData.vi"/>
				<Item Name="DN4_AI_ReadRawData16.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_ReadRawData16.vi"/>
				<Item Name="DN4_AI_ReadRawData32.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_ReadRawData32.vi"/>
				<Item Name="DN4_AI_ReadScaledData.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_ReadScaledData.vi"/>
				<Item Name="DN4_AI_StreamingBufferedRead.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AI_StreamingBufferedRead.vi"/>
				<Item Name="DN4_AO_InstantWrite.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_InstantWrite.vi"/>
				<Item Name="DN4_AO_OneBufferedWrite.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_OneBufferedWrite.vi"/>
				<Item Name="DN4_AO_StreamingBufferedWrite.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_StreamingBufferedWrite.vi"/>
				<Item Name="DN4_AO_WriteData.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_WriteData.vi"/>
				<Item Name="DN4_AO_WriteRawData16.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_WriteRawData16.vi"/>
				<Item Name="DN4_AO_WriteRawData32.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_WriteRawData32.vi"/>
				<Item Name="DN4_AO_WriteScaledData.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_AO_WriteScaledData.vi"/>
				<Item Name="DN4_BfdCntr_ECRead.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_BfdCntr_ECRead.vi"/>
				<Item Name="DN4_BfdCntr_PwMeterRead.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_BfdCntr_PwMeterRead.vi"/>
				<Item Name="DN4_BfdCntr_PwModulatorWrite.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_BfdCntr_PwModulatorWrite.vi"/>
				<Item Name="DN4_BfdCntr_UDRead.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_BfdCntr_UDRead.vi"/>
				<Item Name="DN4_BufferedAI_GetData.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_BufferedAI_GetData.vi"/>
				<Item Name="DN4_BufferedAO_SetData.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_BufferedAO_SetData.vi"/>
				<Item Name="DN4_ContinueCompare_DetectEvent_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_ContinueCompare_DetectEvent_Ex.vi"/>
				<Item Name="DN4_ContinueCompare_SetCompareTable_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_ContinueCompare_SetCompareTable_Ex.vi"/>
				<Item Name="DN4_ControlEndTask.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_ControlEndTask.vi"/>
				<Item Name="DN4_ControlSetConfig.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_ControlSetConfig.vi"/>
				<Item Name="DN4_ControlStart.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_ControlStart.vi"/>
				<Item Name="DN4_ControlStop.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_ControlStop.vi"/>
				<Item Name="DN4_Counter_DetectCntrEvent_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_Counter_DetectCntrEvent_Ex.vi"/>
				<Item Name="DN4_Counter_ReadPulseWidth_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_Counter_ReadPulseWidth_Ex.vi"/>
				<Item Name="DN4_Counter_RegisterLVEvent.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_Counter_RegisterLVEvent.vi"/>
				<Item Name="DN4_Counter_SetDelayCount_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_Counter_SetDelayCount_Ex.vi"/>
				<Item Name="DN4_Counter_SetFrequency_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_Counter_SetFrequency_Ex.vi"/>
				<Item Name="DN4_Counter_SetPulseWidth_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_Counter_SetPulseWidth_Ex.vi"/>
				<Item Name="DN4_DDT21Darray.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_DDT21Darray.vi"/>
				<Item Name="DN4_DDT22Darray.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_DDT22Darray.vi"/>
				<Item Name="DN4_DI_DetectInterrupt.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_DI_DetectInterrupt.vi"/>
				<Item Name="DN4_DI_Read.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_DI_Read.vi"/>
				<Item Name="DN4_DI_RegisterEvent.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_DI_RegisterEvent.vi"/>
				<Item Name="DN4_DO_Write.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_DO_Write.vi"/>
				<Item Name="DN4_EventCounting_GetValue_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_EventCounting_GetValue_Ex.vi"/>
				<Item Name="DN4_FreqMeter_ReadValue_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_FreqMeter_ReadValue_Ex.vi"/>
				<Item Name="DN4_GetAnalogDataType.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetAnalogDataType.vi"/>
				<Item Name="DN4_GetBufferedSize.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetBufferedSize.vi"/>
				<Item Name="DN4_GetChannelCount.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetChannelCount.vi"/>
				<Item Name="DN4_GetDataSize.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetDataSize.vi"/>
				<Item Name="DN4_GetRate.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetRate.vi"/>
				<Item Name="DN4_GetSamples.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetSamples.vi"/>
				<Item Name="DN4_GetTimeout.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_GetTimeout.vi"/>
				<Item Name="DN4_SetRate.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_SetRate.vi"/>
				<Item Name="DN4_SetSamples.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_SetSamples.vi"/>
				<Item Name="DN4_SetTimeOut.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_SetTimeOut.vi"/>
				<Item Name="DN4_SnapCounter_DetectEvent.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_SnapCounter_DetectEvent.vi"/>
				<Item Name="DN4_SnapCounter_SetEventsToSnap.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_SnapCounter_SetEventsToSnap.vi"/>
				<Item Name="DN4_UdCounter_RegisterLVEvent.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_UdCounter_RegisterLVEvent.vi"/>
				<Item Name="DN4_UpDownCounter_GetValue_Ex.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/DN4_UpDownCounter_GetValue_Ex.vi"/>
				<Item Name="GetTaskTypeByTaskID.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/GetTaskTypeByTaskID.vi"/>
				<Item Name="openg_error.lvlib" Type="Library" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/openg_error.lvlib"/>
				<Item Name="openg_variant.lvlib" Type="Library" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/openg_variant.lvlib"/>
				<Item Name="SetDevice.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/SetDevice.vi"/>
				<Item Name="subDAQNaviAssistant2.vi" Type="VI" URL="/&lt;userlib&gt;/_express/DAQNaviAssistantSource_v002.llb/subDAQNaviAssistant2.vi"/>
				<Item Name="ToErrorCluster.vi" Type="VI" URL="/&lt;userlib&gt;/_express/AdvComponent/ToErrorCluster.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Dynamic To Waveform Array.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Dynamic To Waveform Array.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="JKI JSON Serialization.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI.lib/Serialization/JSON/JKI JSON Serialization.lvlib"/>
				<Item Name="JKI Serialization.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI.lib/Serialization/Core/JKI Serialization.lvlib"/>
				<Item Name="LabVIEWHTTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/httpClient/LabVIEWHTTPClient.lvlib"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LV70DateRecToTimeStamp.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/LV70DateRecToTimeStamp.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NATS Core.lvlib" Type="Library" URL="/&lt;vilib&gt;/NATS/NATS Core/NATS Core.lvlib"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="Path to URL inner.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL inner.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Qualified Name Array To Single String.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace One-Sided.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace One-Sided.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Waveform Array To Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Waveform Array To Dynamic.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AdvLvDaq.dll" Type="Document" URL="../../../../../../../../Windows/System32/AdvLvDaq.dll"/>
			<Item Name="DMM_MUX_complete.vi" Type="VI" URL="../SUBVIs/DMM_Switch_combo/DMM_MUX_complete.vi"/>
			<Item Name="DMM_Swith_StateMachine.vi" Type="VI" URL="../SUBVIs/DMM_Switch_combo/DMM_Swith_StateMachine.vi"/>
			<Item Name="execute_a_specific_Test.vi" Type="VI" URL="../SUBVIs/execute_a_specific_Test.vi"/>
			<Item Name="GUID_rev0.vi" Type="VI" URL="../../RiDE_LV/subvis/GUID_rev0.vi"/>
			<Item Name="HTTP_POST_RiDE_Result.vi" Type="VI" URL="../../RiDE_LV/subvis/HTTP_POST_RiDE_Result.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="MotorControl_ReportResult_RiDE.vi" Type="VI" URL="../SUBVIs/MotorControl_ReportResult_RiDE.vi"/>
			<Item Name="ni_dmm_close.vi" Type="VI" URL="../SUBVIs/DMM/ni_dmm_close.vi"/>
			<Item Name="ni_dmm_init.vi" Type="VI" URL="../SUBVIs/DMM/ni_dmm_init.vi"/>
			<Item Name="ni_dmm_measure.vi" Type="VI" URL="../SUBVIs/DMM/ni_dmm_measure.vi"/>
			<Item Name="ni_dmm_measure_rev01.vi" Type="VI" URL="../SUBVIs/DMM/ni_dmm_measure_rev01.vi"/>
			<Item Name="NI_Switch_auto_close.vi" Type="VI" URL="../SUBVIs/Switch/autosequence/NI_Switch_auto_close.vi"/>
			<Item Name="NI_Switch_auto_Init.vi" Type="VI" URL="../SUBVIs/Switch/autosequence/NI_Switch_auto_Init.vi"/>
			<Item Name="NI_Switch_auto_trigger_Path.vi" Type="VI" URL="../SUBVIs/Switch/autosequence/NI_Switch_auto_trigger_Path.vi"/>
			<Item Name="NI_Switch_Close.vi" Type="VI" URL="../SUBVIs/Switch/NI_Switch_Close.vi"/>
			<Item Name="NI_Switch_CreatePath.vi" Type="VI" URL="../SUBVIs/Switch/NI_Switch_CreatePath.vi"/>
			<Item Name="NI_Switch_Init.vi" Type="VI" URL="../SUBVIs/Switch/NI_Switch_Init.vi"/>
			<Item Name="nidmm_64.dll" Type="Document" URL="nidmm_64.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niswitch_64.dll" Type="Document" URL="niswitch_64.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="R_I_Check_To_RiDE.vi" Type="VI" URL="../SUBVIs/DMM_Switch_combo/DMM_MUX_Loop/R_I_Check_To_RiDE.vi"/>
			<Item Name="Serial - Settings.ctl" Type="VI" URL="../Serial - Settings.ctl"/>
			<Item Name="stringToNum.vi" Type="VI" URL="../SUBVIs/stringToNum.vi"/>
			<Item Name="Testelement_to_Instrument.vi" Type="VI" URL="../SUBVIs/Testelement_to_Instrument.vi"/>
			<Item Name="time_to_float_rev0.vi" Type="VI" URL="../../RiDE_LV/subvis/time_to_float_rev0.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="dmm_mux_continuous_mode" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{3CDBD6E5-CDD0-4B3A-885C-3630DFCFC33C}</Property>
				<Property Name="App_INI_GUID" Type="Str">{3655ED4B-CD24-492E-93C0-96731E70985E}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{CB442086-ED3A-45F8-9F22-BFEC08A68725}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">dmm_mux_continuous_mode</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/dmm_mux_continuous_mode</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{F939A773-569A-4541-817F-49553C03A3DC}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/dmm_mux_continuous_mode/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/dmm_mux_continuous_mode/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{6D7B1892-A866-4090-9559-A744527EE644}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">dmm_mux_continuous_mode</Property>
				<Property Name="TgtF_internalName" Type="Str">dmm_mux_continuous_mode</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">dmm_mux_continuous_mode</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A04237A5-F90F-496D-A8EC-ABFFF57B7221}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Common_Rev10" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{028464B2-E27D-4E8B-ACE0-216DC1342CF9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D65AEFF4-445E-4E32-9ADA-8EEA6A60C6A8}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F231F2EC-6B5E-4255-BFED-B4FF17407CAD}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Common_Rev10</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev10_Common</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{F7F831AC-B828-4E1F-8D86-D4D90971E6E4}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Common_Rev10.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev10_Common/GA_Doorline_Common_Rev10.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev10_Common/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{E4BD2ED6-E61E-4ECB-90F1-E37878180524}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Common_Rev10</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Common_Rev10</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Common_Rev10</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{69EF3AE7-5B15-4C95-BDAE-9B5B8D8659C7}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Common_Rev10.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Common_Rev11" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{16D89EA2-0537-45A7-9391-A6D9CAEECDCA}</Property>
				<Property Name="App_INI_GUID" Type="Str">{33E151B7-FCC6-4C34-9D66-C301891F6A6B}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{FB62510F-5CA4-4A86-B18B-D5B4B4DBE345}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Rev 11 : fixed "run all tests" persistance. now it stays on till end of all the tests. </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Common_Rev11</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev11_Common_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{EECFFA83-6CC7-4E75-B851-6730001ECFDD}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Common_Rev11.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev11_Common_UI/GA_Doorline_Common_Rev11.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev11_Common_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{E4BD2ED6-E61E-4ECB-90F1-E37878180524}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Common_Rev11</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Common_Rev11</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Common_Rev11</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{C8AA69BA-4535-4916-9FF6-933378414CFB}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Common_Rev11.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Common_Rev12" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{6B3FC440-22A7-4950-AFC2-49F6140B6471}</Property>
				<Property Name="App_INI_GUID" Type="Str">{1FBC8957-BD17-497A-BB81-4D5330F4572D}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C47B6B45-F876-4DD7-AF8B-B5FD1386C420}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Static IP Door lineside IP assigned instead of the RiDE URL</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Common_Rev12</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev12_Common_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{19D8C289-38F7-405B-B891-4CB9B7EB311F}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Common_Rev12.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev12_Common_UI/GA_Doorline_Common_Rev12.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev12_Common_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{DCABB046-E978-470F-A283-04AA31232ABE}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Common_Rev12</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Common_Rev12</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Common_Rev12</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{AF2E8A63-682B-4F66-AA2A-46ABDAE3C65B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Common_Rev12.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Left__Rev10" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{67561CC6-3475-4B66-A491-13CC543342BA}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{45E21CB8-05D7-4E12-B56D-2DDF6EC5B1CB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-488.2 Runtime 23.5</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{DBE556BD-D918-435A-9B63-8E386E86BF90}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-DMM Runtime 23.8 for NI 407x and NI 4065 Devices</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{897361D3-FA22-4FAB-B6BE-81688F5CD73D}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C80B85D2-02A3-4335-B03F-413BA0EC2747}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI-DMM Runtime 23.8 for NI 408x Devices</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{A2D09DEE-B414-4610-8D52-B5932C463912}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{A2CF5554-734D-4D8B-98FC-F4B9DE29ECDC}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI-Serial Runtime 23.8</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{AB330669-9521-40B6-9E16-940AC2A5E6CF}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-SWITCH Runtime 23.8 for SwitchCA1, 2 &amp; 3 Devices (NI-DAQmx)</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{CF043E95-1F73-41C0-9026-8F59342B068C}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{58B539BE-6D43-4917-BDC3-2788BEC26CB5}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-SWITCH Runtime 23.8 for SwitchCA4 Devices</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{F5BA5FC9-7878-49A9-B54E-D284078C407D}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{57642ED1-74E5-4FA3-9407-9499E414A555}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-VISA Runtime 24.0</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[7].productID" Type="Str">{34D9C224-BFA4-4288-9226-EAF4EEDC48BD}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI LabVIEW Runtime 2024 Q1 Patch 1 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[0].productName" Type="Str">NI ActiveX Container (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[7].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[1].productName" Type="Str">NI Deployment Framework 2024 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[1].upgradeCode" Type="Str">{E0D3C7F9-4512-438F-8123-E2050457972B}</Property>
				<Property Name="DistPart[7].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[10].productName" Type="Str">NI TDM Streaming 24.1</Property>
				<Property Name="DistPart[7].SoftDep[10].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[7].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[2].productName" Type="Str">NI Error Reporting 2020 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[2].upgradeCode" Type="Str">{785BE224-E5B2-46A5-ADCB-55C949B5C9C7}</Property>
				<Property Name="DistPart[7].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2024</Property>
				<Property Name="DistPart[7].SoftDep[3].upgradeCode" Type="Str">{57233740-EFE9-3C47-BF6A-4C5981105136}</Property>
				<Property Name="DistPart[7].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[4].productName" Type="Str">NI Logos 24.1</Property>
				<Property Name="DistPart[7].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[7].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[5].productName" Type="Str">NI LabVIEW Web Server 2024 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[5].upgradeCode" Type="Str">{5F449D4C-83B9-492E-986B-6B85A29C431D}</Property>
				<Property Name="DistPart[7].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[6].productName" Type="Str">NI mDNS Responder 24.0</Property>
				<Property Name="DistPart[7].SoftDep[6].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[7].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[7].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[7].SoftDep[7].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[7].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2020</Property>
				<Property Name="DistPart[7].SoftDep[8].upgradeCode" Type="Str">{9872BBBA-FB96-42A4-80A2-9605AC5CBCF1}</Property>
				<Property Name="DistPart[7].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[7].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[7].SoftDepCount" Type="Int">11</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{B2695A3E-34C2-3082-9B16-BB16F4DF1A07}</Property>
				<Property Name="DistPartCount" Type="Int">8</Property>
				<Property Name="INST_author" Type="Str">Rivian</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/Installers/left_rev10</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">GA_Doorline_Left__Rev10</Property>
				<Property Name="INST_defaultDir" Type="Str">{67561CC6-3475-4B66-A491-13CC543342BA}</Property>
				<Property Name="INST_installerName" Type="Str">GA_Doorline_Left__Rev10.exe</Property>
				<Property Name="INST_productName" Type="Str">GA_Doorline_Left__Rev10</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.1</Property>
				<Property Name="InstSpecBitness" Type="Str">64-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">24118001</Property>
				<Property Name="MSI_arpCompany" Type="Str">Rivian</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.rivian.com/</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{E1C1C1B5-A943-4C0E-A710-0CCD7AB919F9}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{8AA3776D-46B3-4006-BAF1-C6DF2F7A6815}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{67561CC6-3475-4B66-A491-13CC543342BA}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{67561CC6-3475-4B66-A491-13CC543342BA}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">Rev10_LeftDoor_UI.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Rev10_LeftDoor_UI</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{8CF0C407-676B-4C68-8E75-A78B2159AF5C}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">Rev10_LeftDoor_UI</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/Rev10_LeftDoor_UI</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="GA_Doorline_Left_Rev04" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{F3330531-722D-499A-BD8F-97AB5515BE40}</Property>
				<Property Name="App_INI_GUID" Type="Str">{DD80F058-39AD-411D-A4FE-1FA5900D85C9}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C7C01B31-53B2-40C1-8FBA-65A4FBF5DA24}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Left_Rev04</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev4_LeftDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E3D92218-F43F-424B-BE02-EBDB82A57035}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Left_Rev04.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev4_LeftDoor_UI/GA_Doorline_Left_Rev04.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev4_LeftDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{1756F7A6-F996-4466-968D-9F9368A608FE}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Left_Rev04</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Left_Rev04</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Left_Rev04</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{23BB0A5F-F759-4BA0-B205-13FA92361DAD}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Left_Rev04.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Left_Rev07" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{F1DB67B1-2C8B-4818-A5AD-012E6177EC5E}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A287845D-423B-4703-BEB7-F45BD09190C7}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{337766D0-B07C-48C7-A828-EEFBE683F5F1}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Left_Rev07</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev07_LeftDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C3BFAECD-C3B9-42FB-BF9D-ACD4D9433243}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Left_Rev07.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev07_LeftDoor_UI/GA_Doorline_Left_Rev07.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev07_LeftDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{54CA04FA-BE87-4EC3-8590-FD46EE6960C8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Left_Rev07</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Left_Rev07</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Left_Rev07</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B9429073-3117-4370-8C38-D6E4E80D644A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Left_Rev07.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Left_Rev08" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{4FCDB64D-E293-4405-B5E8-1A7E282AA8E8}</Property>
				<Property Name="App_INI_GUID" Type="Str">{3A636076-8B7A-471A-A4ED-075094EF30A8}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{6F76225B-D76F-4DA3-9107-E2DA663E3860}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">RI went from 6 to 5 iterations , added VIN based RiDE publish to RI check </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Left_Rev08</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev08_LeftDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{9235A6A6-F6E9-466D-8EA8-11DB9A0832C8}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Left_Rev08.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev08_LeftDoor_UI/GA_Doorline_Left_Rev08.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev08_LeftDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{54CA04FA-BE87-4EC3-8590-FD46EE6960C8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Left_Rev08</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Left_Rev08</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Left_Rev08</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{18564950-8DE3-4652-A89D-4705E7922F56}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Left_Rev08.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Left_Rev09" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{EFD48F23-68A5-4D2D-9037-BAF227F7BD86}</Property>
				<Property Name="App_INI_GUID" Type="Str">{46EC9219-0E89-41D1-A3DC-104BDFEF2613}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{BB4EF3FD-FFC5-49C4-9E26-41ECC337C91F}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">added 75 seconds to motor run </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Left_Rev09</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/GA_Doorline_Left_Rev09</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{2D3EB4FC-0865-4A93-BB57-C60AB1CEDDD1}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Left_Rev09.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/GA_Doorline_Left_Rev09/GA_Doorline_Left_Rev09.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/GA_Doorline_Left_Rev09/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{5B0C061D-70B3-4D7E-9EAC-1A8271B2CA4F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Left_Rev09</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Left_Rev09</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Left_Rev09</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{128BF87E-D68A-4FC3-B45B-48EF8E9BAD89}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Left_Rev09.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev13" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{1B5C7F66-9EAE-490C-A689-C6BAF767B9A8}</Property>
				<Property Name="App_INI_GUID" Type="Str">{E899EDF8-E500-4AC2-B1F5-3781102C9FD4}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{754FEA41-178C-4A39-8895-8F449129BB3C}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Added elatch switch test case for Ajar and release switches </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev13</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev13_Doorline_Common_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C676F860-8C61-49C4-A120-8F17A6DB8290}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev13.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev13_Doorline_Common_UI/GA_Doorline_Rev13.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev13_Doorline_Common_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{07DE7E74-32B9-452D-A947-60C5CA16119C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev13</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev13</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev13</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{59D613D8-EF99-4E53-B996-ADE9244A109F}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev13.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev14" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9FF4F578-B59A-4C07-8129-4868E58BCC6D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{CDA55529-FD51-41F0-BC06-D66FD5150253}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{63084280-6794-4B65-903B-C8707014BBFC}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Start Test button read (from lineside) is integrated, elatch switch test coverage added </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev14</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/GA_Doorline_Rev14</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{18A8AA09-2533-4FB3-B5C0-0E0BB6DBA0DB}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev14.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/GA_Doorline_Rev14/GA_Doorline_Rev14.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/GA_Doorline_Rev14/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{A20F4D1B-AEDB-4ED5-AE0F-83DA01EA1FD9}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev14</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev14</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev14</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{ABA9E58D-64B2-4757-91C8-30C217EB8221}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev14.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev15" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{76A6DB3A-4427-45E8-ADE9-1E56F44915A0}</Property>
				<Property Name="App_INI_GUID" Type="Str">{3FF4F627-3D1D-4261-BD51-99601D0B6EAE}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{A9B14A41-3DD1-4762-B8B9-CA6F3A879186}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Added Hostnames to RiDE task results , Added VIN to Status reason </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev15</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev15_Doorline_Common_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{84866236-99A7-4A6B-867C-AF7A63B08ADB}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev15.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev15_Doorline_Common_UI/GA_Doorline_Rev15.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev15_Doorline_Common_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{ECCEA683-84BB-4238-9848-64B95DF111AC}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev15.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev15</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev15</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev15</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{5A2F2247-AA13-4DBE-A7BF-EF175BA5E0CC}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev15.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev16" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{0114F354-978C-4883-8811-BDE221062D50}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4C170469-6560-4A28-BE18-EB83D5EF1E76}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{2B90F758-ED6C-452C-96C8-D30707492BC2}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Added Persistance to Resistance measurements to try for 3 times. Added Shop name , line name and station name on the doorline </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev16</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6171E538-B170-442F-80F4-FF3B3C263347}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev16.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev16.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{AFE44A9B-1EBB-48BF-BD71-8496A65D39C1}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev16.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev16</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev16</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev16</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{43A7A8A4-478E-4D45-AF35-CF77FAA1FB21}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev16.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev17" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{530A6FEF-3475-4E07-8DB0-842E386D4AFC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{0DC2A239-89B1-4EF3-A9F4-FBB7CDF452C2}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{FDDDD5AF-5056-45FD-A3EC-1917B607D06C}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Rev17 Inclusions :Added routine to clear "RI_Test_Done" flag </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev17</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev17</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{0C3D6DB8-65E0-43FE-9D5A-38556B9AA2B4}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev17.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev17/GA_Doorline_Rev17.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev17/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{3F786011-2EE0-43C4-A3AB-D7504009B062}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev17.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev17</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev17</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev17</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{01E3A922-5102-4418-BC3D-259B2DC1EA88}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev17.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev18" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{576AAF7A-86CB-4556-8553-D8B7C56164FA}</Property>
				<Property Name="App_INI_GUID" Type="Str">{61FDA4CA-A041-432F-83A6-F56ACEAF1772}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F52A7F8D-5C83-4EBE-A5A8-EDC29AFC4E88}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Modified 'hostname', 'station', 'element id'</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev18</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev18</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{288F0567-CE44-4416-84D7-16353FDF011B}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev18.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev18/GA_Doorline_Rev18.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev18/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{A742E010-AC71-4BEF-B2F9-CEF40749228F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev18.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev18</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev18</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev18</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{06D36F7E-CBB6-4AD8-B796-A6D8043805F9}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev18.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev19" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9A886DB5-629C-4026-A307-F5950E520EE6}</Property>
				<Property Name="App_INI_GUID" Type="Str">{AF9D3E6B-E3DA-4826-9D6B-0D0A6ABA4776}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{BB47C9AD-C39B-479D-B361-CA5940947414}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Modified 'element id'</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev19</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev19</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{568A5078-5BDA-449F-8028-E296070A1E29}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev19.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev19/GA_Doorline_Rev19.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev19/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{A742E010-AC71-4BEF-B2F9-CEF40749228F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev19.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev19</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev19</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev19</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{E0A754A7-B150-4246-916F-0EDD97844F53}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev19.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev20" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{B8F785E7-18F3-4D08-A02C-91F22D1C6010}</Property>
				<Property Name="App_INI_GUID" Type="Str">{9074B8F6-1B4F-4722-BD4B-FDD8AE05AAFB}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{9B657447-9235-4445-8F36-D0C9F7E80752}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Modified nats stream names 
// Task result streams left door 
tap.lineside.write.results.DLElectricalTesterDataL.taskresult1
tap.lineside.write.results.DLElectricalTesterDataL.taskresult2
// Task result streams right door 
tap.lineside.write.results.DLElectricalTesterDataL.taskresult1
tap.lineside.write.results.DLElectricalTesterDataR.taskresult2

// Live data streams 

tap.lineside.write.livedata.rightdoor.rawdata
tap.lineside.write.livedata.leftdoor.rawdata

exposed nats loop timers </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev20</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev20</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{37517FA8-E77E-42D5-8B58-F1C667F8E290}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev20.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev20/GA_Doorline_Rev20.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev20/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C71FCC29-5FBB-4643-9580-36587093F714}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev20.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev20</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev20</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev20</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{3DF3510C-B724-495C-BE27-217E8CB81E92}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev20.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev21" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{CCABE97C-25D9-4892-B485-F391A6D5D32E}</Property>
				<Property Name="App_INI_GUID" Type="Str">{3EEC68E9-DD84-44E3-B932-CC8383A3C02C}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{FD15C3FC-8949-4D0C-A5D8-C6BDD1F570CC}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Modified NATs out for task results </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev21</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev21</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{5CBDCA05-5274-4258-81AF-6021E23B18BA}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev21.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev21/GA_Doorline_Rev21.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev21/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{28584B37-9539-4F5F-9EA3-5CAFADD9E8F8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev21.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev21</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev21</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev21</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9204BF29-46C0-4EB9-B7C3-23517F0C77A0}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev21.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Rev22" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{16E30BB7-D8E6-437C-A375-9F79FF0C4DF6}</Property>
				<Property Name="App_INI_GUID" Type="Str">{B32E4524-F35D-4E10-91D0-DC8D34113CD3}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{E3C5E408-EFB2-4F70-AB22-08FCAD76AEA3}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Chaned Front and Rear Door speaker resistance limits to 0.5,40 from 0.5 , 20 </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Rev22</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev22</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{79D22B65-36FA-4CE2-AA00-A6CF549A1D4A}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Rev22.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev22/GA_Doorline_Rev22.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/mtatikonda/OneDrive - Rivian Automotive, LLC/IP_DOOR_PROD_CODEBASE_TAGS/Peregrine_GA_InlineTester_Codebase_Rev15/builds/GA_Doorline_Rev22/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{D469B74E-CDDE-4BF7-A33F-38B746078651}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev22.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Rev22</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Rev22</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Rev22</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{BDD033B2-2C35-42DB-966A-3BD3978B1686}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Rev22.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right__Rev10" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{2D29DE77-8DAF-4857-8699-D7E4374600FD}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{45E21CB8-05D7-4E12-B56D-2DDF6EC5B1CB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-488.2 Runtime 23.5</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{DBE556BD-D918-435A-9B63-8E386E86BF90}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-DMM Runtime 23.8 for NI 407x and NI 4065 Devices</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{897361D3-FA22-4FAB-B6BE-81688F5CD73D}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C80B85D2-02A3-4335-B03F-413BA0EC2747}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI-DMM Runtime 23.8 for NI 408x Devices</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{A2D09DEE-B414-4610-8D52-B5932C463912}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{A2CF5554-734D-4D8B-98FC-F4B9DE29ECDC}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI-Serial Runtime 23.8</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{AB330669-9521-40B6-9E16-940AC2A5E6CF}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-SWITCH Runtime 23.8 for SwitchCA1, 2 &amp; 3 Devices (NI-DAQmx)</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{CF043E95-1F73-41C0-9026-8F59342B068C}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{58B539BE-6D43-4917-BDC3-2788BEC26CB5}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-SWITCH Runtime 23.8 for SwitchCA4 Devices</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{F5BA5FC9-7878-49A9-B54E-D284078C407D}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{57642ED1-74E5-4FA3-9407-9499E414A555}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-VISA Runtime 24.0</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[7].productID" Type="Str">{34D9C224-BFA4-4288-9226-EAF4EEDC48BD}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI LabVIEW Runtime 2024 Q1 Patch 1 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[0].productName" Type="Str">NI ActiveX Container (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[7].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[1].productName" Type="Str">NI Deployment Framework 2024 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[1].upgradeCode" Type="Str">{E0D3C7F9-4512-438F-8123-E2050457972B}</Property>
				<Property Name="DistPart[7].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[10].productName" Type="Str">NI TDM Streaming 24.1</Property>
				<Property Name="DistPart[7].SoftDep[10].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[7].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[2].productName" Type="Str">NI Error Reporting 2020 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[2].upgradeCode" Type="Str">{785BE224-E5B2-46A5-ADCB-55C949B5C9C7}</Property>
				<Property Name="DistPart[7].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2024</Property>
				<Property Name="DistPart[7].SoftDep[3].upgradeCode" Type="Str">{57233740-EFE9-3C47-BF6A-4C5981105136}</Property>
				<Property Name="DistPart[7].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[4].productName" Type="Str">NI Logos 24.1</Property>
				<Property Name="DistPart[7].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[7].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[5].productName" Type="Str">NI LabVIEW Web Server 2024 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[5].upgradeCode" Type="Str">{5F449D4C-83B9-492E-986B-6B85A29C431D}</Property>
				<Property Name="DistPart[7].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[6].productName" Type="Str">NI mDNS Responder 24.0</Property>
				<Property Name="DistPart[7].SoftDep[6].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[7].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[7].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[7].SoftDep[7].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[7].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2020</Property>
				<Property Name="DistPart[7].SoftDep[8].upgradeCode" Type="Str">{9872BBBA-FB96-42A4-80A2-9605AC5CBCF1}</Property>
				<Property Name="DistPart[7].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[7].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[7].SoftDepCount" Type="Int">11</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{B2695A3E-34C2-3082-9B16-BB16F4DF1A07}</Property>
				<Property Name="DistPartCount" Type="Int">8</Property>
				<Property Name="INST_author" Type="Str">Rivian</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/Installers/right_rev10</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">GA_Doorline_Right__Rev10</Property>
				<Property Name="INST_defaultDir" Type="Str">{2D29DE77-8DAF-4857-8699-D7E4374600FD}</Property>
				<Property Name="INST_installerName" Type="Str">GA_Doorline_Right__Rev10.exe</Property>
				<Property Name="INST_productName" Type="Str">GA_Doorline_Right__Rev10</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.1</Property>
				<Property Name="InstSpecBitness" Type="Str">64-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">24118001</Property>
				<Property Name="MSI_arpCompany" Type="Str">Rivian</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.rivian.com/</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{2CF93B18-27D4-46F8-AB68-873C5529A432}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{989D4670-CC68-406E-96E1-E3612864B522}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{2D29DE77-8DAF-4857-8699-D7E4374600FD}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{2D29DE77-8DAF-4857-8699-D7E4374600FD}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">GA_Doorline_Right_Rev10.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">GA_Doorline_Right_Rev10</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{E25A349C-F0B1-48B2-AC7F-34963856FF76}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">GA_Doorline_Right_Rev10</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/GA_Doorline_Right_Rev10</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Installer_Rev09" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{EE5BD06B-D053-4430-AA5E-DFA60D7C8EC9}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{45E21CB8-05D7-4E12-B56D-2DDF6EC5B1CB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-488.2 Runtime 23.5</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{DBE556BD-D918-435A-9B63-8E386E86BF90}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-DMM Runtime 23.8 for NI 407x and NI 4065 Devices</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{897361D3-FA22-4FAB-B6BE-81688F5CD73D}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C80B85D2-02A3-4335-B03F-413BA0EC2747}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI-DMM Runtime 23.8 for NI 408x Devices</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{A2D09DEE-B414-4610-8D52-B5932C463912}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{A2CF5554-734D-4D8B-98FC-F4B9DE29ECDC}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI-Serial Runtime 23.8</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{AB330669-9521-40B6-9E16-940AC2A5E6CF}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-SWITCH Runtime 23.8 for SwitchCA1, 2 &amp; 3 Devices (NI-DAQmx)</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{CF043E95-1F73-41C0-9026-8F59342B068C}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{58B539BE-6D43-4917-BDC3-2788BEC26CB5}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-SWITCH Runtime 23.8 for SwitchCA4 Devices</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{F5BA5FC9-7878-49A9-B54E-D284078C407D}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{57642ED1-74E5-4FA3-9407-9499E414A555}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-VISA Runtime 24.0</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[7].productID" Type="Str">{34D9C224-BFA4-4288-9226-EAF4EEDC48BD}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI LabVIEW Runtime 2024 Q1 Patch 1 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[0].productName" Type="Str">NI ActiveX Container (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[7].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[1].productName" Type="Str">NI Deployment Framework 2024 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[1].upgradeCode" Type="Str">{E0D3C7F9-4512-438F-8123-E2050457972B}</Property>
				<Property Name="DistPart[7].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[10].productName" Type="Str">NI TDM Streaming 24.1</Property>
				<Property Name="DistPart[7].SoftDep[10].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[7].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[2].productName" Type="Str">NI Error Reporting 2020 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[2].upgradeCode" Type="Str">{785BE224-E5B2-46A5-ADCB-55C949B5C9C7}</Property>
				<Property Name="DistPart[7].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2024</Property>
				<Property Name="DistPart[7].SoftDep[3].upgradeCode" Type="Str">{57233740-EFE9-3C47-BF6A-4C5981105136}</Property>
				<Property Name="DistPart[7].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[4].productName" Type="Str">NI Logos 24.1</Property>
				<Property Name="DistPart[7].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[7].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[5].productName" Type="Str">NI LabVIEW Web Server 2024 (64-bit)</Property>
				<Property Name="DistPart[7].SoftDep[5].upgradeCode" Type="Str">{5F449D4C-83B9-492E-986B-6B85A29C431D}</Property>
				<Property Name="DistPart[7].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[6].productName" Type="Str">NI mDNS Responder 24.0</Property>
				<Property Name="DistPart[7].SoftDep[6].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[7].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[7].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[7].SoftDep[7].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[7].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2020</Property>
				<Property Name="DistPart[7].SoftDep[8].upgradeCode" Type="Str">{9872BBBA-FB96-42A4-80A2-9605AC5CBCF1}</Property>
				<Property Name="DistPart[7].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[7].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[7].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[7].SoftDepCount" Type="Int">11</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{B2695A3E-34C2-3082-9B16-BB16F4DF1A07}</Property>
				<Property Name="DistPartCount" Type="Int">8</Property>
				<Property Name="INST_author" Type="Str">Rivian</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/Installers</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">GA_Doorline_Right_Installer_Rev09</Property>
				<Property Name="INST_defaultDir" Type="Str">{EE5BD06B-D053-4430-AA5E-DFA60D7C8EC9}</Property>
				<Property Name="INST_installerName" Type="Str">GA_Doorline_Right_Installer_Rev09.exe</Property>
				<Property Name="INST_productName" Type="Str">GA_Doorline_Right_Installer_Rev09</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.1</Property>
				<Property Name="InstSpecBitness" Type="Str">64-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">24118001</Property>
				<Property Name="MSI_arpCompany" Type="Str">Rivian</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.rivian.com/</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{51256DB0-435A-4C85-9C23-5CC084CC4211}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{D6D91E66-8D0E-4873-B08C-2E77BB94F47F}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{EE5BD06B-D053-4430-AA5E-DFA60D7C8EC9}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{EE5BD06B-D053-4430-AA5E-DFA60D7C8EC9}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">GA_Doorline_Right_Rev09.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">GA_Doorline_Right_Rev09</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{76E48EA4-FA20-4AA3-8D95-07B678AED3C5}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">GA_Doorline_Right_Rev09</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/GA_Doorline_Right_Rev09</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev04" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{D2F322D0-EAC4-407C-BB6A-ACE5B1ACB05F}</Property>
				<Property Name="App_INI_GUID" Type="Str">{7B3FEE48-440B-4324-BFC2-BC2206D96DD9}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{15FF8C06-C957-4DDC-85D2-B97D584D9470}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Right door UI - Com18 </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev04</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev4_RightDoor_UII</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B71E0FCA-9F86-4685-AB86-023D75A28F1A}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev04.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev4_RightDoor_UII/GA_Doorline_Right_Rev04.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev4_RightDoor_UII/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{1756F7A6-F996-4466-968D-9F9368A608FE}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev04</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev04</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev04</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F6B81496-790C-4820-99F6-5190BC1BF6FD}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev04.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev05" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{D86AA49A-B210-4F39-AA75-54B68C0E7596}</Property>
				<Property Name="App_INI_GUID" Type="Str">{EC88B99C-B7D8-463D-9FDC-EF6D26EFCBC6}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{DB55E5EB-6E5E-4E12-B20F-A8C6156331A6}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Adding NATs server for the doorline </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev05</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev05_RightDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E0721B95-52C2-4C43-BD2E-A34E88A09B88}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev05.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev05_RightDoor_UI/GA_Doorline_Right_Rev05.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev05_RightDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{1756F7A6-F996-4466-968D-9F9368A608FE}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev05</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev05</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev05</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9371D928-73BD-444E-854D-EBE153323C55}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev05.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev06" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{1B267005-B3F3-4C1A-A086-6064F3A5F0D9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{52CA3EC1-A722-4810-ABE4-661827B518A7}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{93AB623B-BD6D-4E8D-A4CB-B14D8E25188D}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">adding run a specific test from the manual control panel </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev06</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev06_RightDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{0CB5F432-690F-4285-B4D9-9D525D2DDBB1}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev06.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev06_RightDoor_UI/GA_Doorline_Right_Rev06.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev06_RightDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{54CA04FA-BE87-4EC3-8590-FD46EE6960C8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev06</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev06</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev06</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B754870C-1937-4732-8466-3FB42F562D32}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev06.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev07" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{2CC5D76D-F48A-4373-95B8-8420CABD4A32}</Property>
				<Property Name="App_INI_GUID" Type="Str">{FFB0A5AD-5948-4237-8011-F336B838A9C6}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{0CC58990-DD51-46B1-BA3C-3B74E6F0AEBA}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev07</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev07_RightDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{60C28641-97DE-4C2C-96BA-3D2B6FD435CE}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev07.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev07_RightDoor_UI/GA_Doorline_Right_Rev07.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev07_RightDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{54CA04FA-BE87-4EC3-8590-FD46EE6960C8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev07</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev07</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev07</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{C65E3C8E-D3AC-40A4-BCDF-E272ED2B9130}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev07.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev08" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{8F8E94B3-ACA5-4E2A-A3F3-1615DBDDDFC9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{7B001AE5-10FC-40EA-A965-05F34CD09C09}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{102A5AED-E7CF-4D1B-9F20-13EBB06178D5}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev08</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev08_RightDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4EF1894C-24AD-441A-8665-4B976BE4585C}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev08.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev08_RightDoor_UI/GA_Doorline_Right_Rev08.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev08_RightDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{54CA04FA-BE87-4EC3-8590-FD46EE6960C8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev08</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev08</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev08</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{C18BDFFF-28A8-4554-B55E-66EC9BE22DD9}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev08.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev09" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9170CB85-76F1-4570-8539-AB7900FB7136}</Property>
				<Property Name="App_INI_GUID" Type="Str">{9B80A75A-2C39-426C-911A-26204021E6FE}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{2EBA0FBF-7E3D-454F-A5F2-8F7911D91718}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Motor test wait time changed to 75 seconds </Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev09</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev09_RightDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{52E42FA5-A123-4356-B0FA-9CFFB118DEB0}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev09.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev09_RightDoor_UI/GA_Doorline_Right_Rev09.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev09_RightDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{5B0C061D-70B3-4D7E-9EAC-1A8271B2CA4F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev09</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev09</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev09</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{76E48EA4-FA20-4AA3-8D95-07B678AED3C5}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev09.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GA_Doorline_Right_Rev10" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{390B00BF-70B2-4C5F-8317-8C66BA60372B}</Property>
				<Property Name="App_INI_GUID" Type="Str">{31AEEA79-FDE9-4204-8D92-5F2672B4BD75}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{24E26654-C05A-4841-9C76-29129B9AA8A8}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Rev10 - com port set to 09 on right door and added nats publisher</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GA_Doorline_Right_Rev10</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev10_RightDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E55D57D8-F791-42B7-805F-B50693D95E4B}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GA_Doorline_Right_Rev10.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev10_RightDoor_UI/GA_Doorline_Right_Rev10.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev10_RightDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{5631356F-C1F9-469D-B919-72E04EAD6E00}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GA_Doorline_Right_Rev10</Property>
				<Property Name="TgtF_internalName" Type="Str">GA_Doorline_Right_Rev10</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">GA_Doorline_Right_Rev10</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{E25A349C-F0B1-48B2-AC7F-34963856FF76}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GA_Doorline_Right_Rev10.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Peregrine_GA_Prod_OperatorUI" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{1E2896AB-EFF7-4DD0-B1FF-F017B94C447D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{59DF3FEB-3396-4327-AE62-5467805A8162}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{A960B514-C59E-44A4-9AF3-43DA0DD59290}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C5457E8E-A122-461E-B25E-46FECC8EFF9A}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{5B0C061D-70B3-4D7E-9EAC-1A8271B2CA4F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="TgtF_internalName" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Peregrine_GA_Prod_OperatorUI</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B0DE15B5-5B6C-47F0-8EA0-C8460548E65D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Peregrine_GA_Prod_OperatorUI2" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{7F037627-64A1-4270-8976-5AAAEF8676C6}</Property>
				<Property Name="App_INI_GUID" Type="Str">{1563DFF1-CBB9-4D56-AD2E-D094D02030B9}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{0D6554E7-DD89-45EE-915B-FF6C0089EDA4}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Peregrine_GA_Prod_OperatorUI2</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI2</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{FACB5B43-8F68-4299-9C4A-7DC87FCDF529}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI2/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI2/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{07DE7E74-32B9-452D-A947-60C5CA16119C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Peregrine_GA_Prod_OperatorUI2</Property>
				<Property Name="TgtF_internalName" Type="Str">Peregrine_GA_Prod_OperatorUI2</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Peregrine_GA_Prod_OperatorUI2</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{037FCEA8-1FF3-45AC-A7D8-6E25D38DF39A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Peregrine_GA_Prod_OperatorUI3" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{ED9FF5EF-1495-4428-A0F4-7B4E65B16550}</Property>
				<Property Name="App_INI_GUID" Type="Str">{35A713E0-5CFD-4B3A-B753-1301D33E87B3}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{CA3E9673-2677-422B-B355-A81024228B93}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Peregrine_GA_Prod_OperatorUI3</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI3</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{1E33A5A7-97EE-42BA-88EC-288109D971BF}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI3/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI3/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{A20F4D1B-AEDB-4ED5-AE0F-83DA01EA1FD9}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Peregrine_GA_Prod_OperatorUI3</Property>
				<Property Name="TgtF_internalName" Type="Str">Peregrine_GA_Prod_OperatorUI3</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Peregrine_GA_Prod_OperatorUI3</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{466B4CC2-7BCF-4CD7-87B2-74BAA3E84B1B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Peregrine_GA_Prod_OperatorUI4" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{8ECBF152-03AF-4318-AD25-F9B788D6789A}</Property>
				<Property Name="App_INI_GUID" Type="Str">{684F5F14-F7CA-491D-9BB5-39BC8CDC980F}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{FB79C86E-D82B-479C-9989-90233B8859F3}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Peregrine_GA_Prod_OperatorUI4</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI4</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{F0B75BEE-9DF8-4D8F-A648-25FA4BC644FA}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI4/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI4/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{A20F4D1B-AEDB-4ED5-AE0F-83DA01EA1FD9}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Peregrine_GA_Prod_OperatorUI4</Property>
				<Property Name="TgtF_internalName" Type="Str">Peregrine_GA_Prod_OperatorUI4</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Peregrine_GA_Prod_OperatorUI4</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{47B60D6C-4E91-49B4-A590-6E850F7930A1}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Peregrine_GA_Prod_OperatorUI_Rev20" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{94779E91-73D7-4959-BB21-3CF46EDBFC82}</Property>
				<Property Name="App_INI_GUID" Type="Str">{6AA17D23-BFB1-4A72-8C77-9A2ED7286303}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{AFDC0A61-902E-4892-9A6F-345AAE0BDD06}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Peregrine_GA_Prod_OperatorUI_Rev20</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI_Rev20</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{DAA34FE2-99AC-47F4-8892-C74E33618DC4}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI_Rev20/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Peregrine_GA_Prod_OperatorUI_Rev20/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C71FCC29-5FBB-4643-9580-36587093F714}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI_Rev20.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Peregrine_GA_Prod_OperatorUI_Rev20</Property>
				<Property Name="TgtF_internalName" Type="Str">Peregrine_GA_Prod_OperatorUI_Rev20</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Peregrine_GA_Prod_OperatorUI_Rev20</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{D0CEDDCA-7D92-4767-B8B1-FDF97826138A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Rev09_Test_LeftDoor" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{785FD310-9B6E-4C45-B9AD-A7C1DACBD2DB}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A94A3BCD-A0DD-487E-9B35-6FB1D75846A4}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{B3CC0C53-D73A-4F91-A282-88DC6530C040}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Rev09_Test_LeftDoor</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev09_TestOnly</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{14C71396-51BE-42EF-9B57-17091AB8BCF8}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Rev09_Test_LeftDoor.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev09_TestOnly/Rev09_Test_LeftDoor.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev09_TestOnly/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{5B0C061D-70B3-4D7E-9EAC-1A8271B2CA4F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Rev09_Test_LeftDoor</Property>
				<Property Name="TgtF_internalName" Type="Str">Rev09_Test_LeftDoor</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Rev09_Test_LeftDoor</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{88654D39-9D50-43B8-8001-C15D1FE28A89}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Rev09_Test_LeftDoor.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="Rev10_LeftDoor_UI" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{AC540055-5DFB-44CD-B1BD-1AE8768FDF6B}</Property>
				<Property Name="App_INI_GUID" Type="Str">{6F1D9E8A-DF2F-452F-9B05-0F3C1D01F93C}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Rivian.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{6F5F7436-C4C8-47B0-BF73-9D616284286E}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Rev10_LeftDoor_UI</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Rev10_LeftDoor_UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{1CD5741D-2E90-454C-92AE-C1BFBD3998DD}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Rev10_LeftDoor_UI.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Rev10_LeftDoor_UI/Rev10_LeftDoor_UI.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Rev10_LeftDoor_UI/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{5631356F-C1F9-469D-B919-72E04EAD6E00}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Peregrine_GA_Prod_OperatorUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Rivian</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Rev10_LeftDoor_UI</Property>
				<Property Name="TgtF_internalName" Type="Str">Rev10_LeftDoor_UI</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2024 Rivian</Property>
				<Property Name="TgtF_productName" Type="Str">Rev10_LeftDoor_UI</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{8CF0C407-676B-4C68-8E75-A78B2159AF5C}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Rev10_LeftDoor_UI.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
